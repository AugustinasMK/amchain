//
// Created by amake on 19/10/17.
//

#include <ctime>
#include <iostream>
#include <utility>
#include "../Headers/Block.h"

Block::Block(const unsigned int version, const vector<Transaction> &transactions) : _header(version), _body(transactions) {
    createMerkle();
}

const string &Block::getHash() const {
    return hash;
}

int Block::MineBlock(unsigned int diff, long maxIterations) {
    string str(diff, '1');
    auto it = 0;
    do {
        _header.setNonce(_header.getNonce() + 1);
        hash = CalculateHash();
        it++;
        if (it >= maxIterations) {
            return -1;
        }
    } while (hash.substr(0, diff) != str);
    std::cout << "Mined " << hash << std::endl;
    return 0;
}

string Block::CalculateHash() {
    std::stringstream ss;
    ss << _header.getVersion() << _header.getTimestamp();
    for (const Transaction& t : _body.getTransactions()){
        ss << t.getId();
    }
    ss << _header.getNonce()<< _header.getPrevBlockHash();
    ss << _header.getMerkleRoot();
    return AMHash::getHash(ss.str());
}

void Block::setPrevHash(const string &h) {
    _header.setPrevBlockHash(h);
}

void Block::createMerkle() {

    auto merkle = vector<string>{};
    merkle.reserve(_body.getTransactions().size());
    for (const Transaction& t : _body.getTransactions()){
        merkle.push_back(t.getId());
    }

    if (merkle.empty()){
        _header.setMerkleRoot("");
        return;
    }
    if (merkle.size() == 1) {
        _header.setMerkleRoot(merkle[0]);
        return;
    }

    while (merkle.size() > 1) {
        if (merkle.size() % 2 == 1) {
            merkle.push_back(merkle.back());
        }
        __glibcxx_assert(merkle.size() % 2 == 0)

        vector<string> nMerkle = vector<string>{};
        nMerkle.reserve(merkle.size() % 2);
        for (int i = 0; i < merkle.size(); i += 2) {
            nMerkle.push_back(AMHash::getHash(merkle[i] + merkle[i + 1]));
        }
        merkle = nMerkle;
    }
    _header.setMerkleRoot(merkle[0]);
}

const string &BlockHeader::getPrevBlockHash() const {
    return prevBlockHash;
}

void BlockHeader::setPrevBlockHash(const string &hash) {
    BlockHeader::prevBlockHash = hash;
}

BlockHeader::BlockHeader(const unsigned int version) : version(version) {
    nonce = -1;
    timestamp = time(nullptr);
}

time_t BlockHeader::getTimestamp() const {
    return timestamp;
}

long BlockHeader::getNonce() const {
    return nonce;
}

unsigned int BlockHeader::getVersion() const {
    return version;
}

void BlockHeader::setNonce(long n) {
    BlockHeader::nonce = n;
}

const string &BlockHeader::getMerkleRoot() const {
    return merkle_root;
}

void BlockHeader::setMerkleRoot(const string &merkleRoot) {
    merkle_root = merkleRoot;
}

BlockBody::BlockBody(vector<Transaction> transactions) : transactions(std::move(transactions)) {}

const vector<Transaction> &BlockBody::getTransactions() const {
    return transactions;
}




