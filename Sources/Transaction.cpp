//
// Created by amake on 19/11/07.
//

#include "../Headers/Transaction.h"

const string &Transaction::getSender() const {
    return sender;
}

void Transaction::setSender(const string &sender) {
    Transaction::sender = sender;
}

const string &Transaction::getReceiver() const {
    return receiver;
}

void Transaction::setReceiver(const string &receiver) {
    Transaction::receiver = receiver;
}

float Transaction::getAmmount() const {
    return ammount;
}

void Transaction::setAmmount(float ammount) {
    Transaction::ammount = ammount;
}

const string &Transaction::getId() const {
    return id;
}

void Transaction::setId(const string &id) {
    Transaction::id = id;
}

time_t Transaction::getDate() const {
    return date;
}

void Transaction::setDate(time_t date) {
    Transaction::date = date;
}
