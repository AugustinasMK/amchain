//
// Created by amake on 19/10/17.
//

#include "../Headers/AMHash.h"

string AMHash::getHash(const string &input) {
    if (input.empty()) {
        std::stringstream ss;
        ss << std::hex << 1152921504606846977;
        return ss.str();
    } else {
        unsigned long long sum = 0;
        int mult = input.size();
        for (char i : input) {
            int t = (int) i;
            sum += t * mult;
            mult--;
        }

        string bit64 = make64Bit(sum, input.size() + 1);
        unsigned long long int int64 = 0;
        std::stringstream ss;
        ss << std::hex << bit64;
        ss >> int64;

        unsigned long long int xmult = 1099511628211;

        for (unsigned char i : input){
            int64 *= xmult;
            int64 ^= i;
        }

        std::stringstream sss;
        sss << std::hex << int64;
        string hash = sss.str();
        return hash;
    }
}

string AMHash::make64Bit(unsigned long long charSum, size_t length) {
    if (charSum <= 1152921504606846977) {
        charSum *= length;
        return make64Bit(charSum, length);
    } else {
        std::stringstream ss;
        ss << std::hex << charSum;
        string s = ss.str();
        return s.substr(0, 16);
    }
}
