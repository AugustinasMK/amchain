#include <iostream>
#include <fstream>
#include <ctime>

#include "../Headers/AMChain.h"
#include "../Headers/User.h"
#include "../Headers/Transaction.h"

long findUserByHash(const vector<User> &users, const string& hash);

int main() {

    AMChain amc = AMChain();

    std::cout << "Hello, World!" << std::endl;

    std::ifstream fsu;
    std::ifstream fst;

    fsu.open("../Users.txt");
    fst.open("../Transactions.txt");

    vector<Transaction> transactions{};
    vector<User> users{};


    for (size_t i = 0; i < 1000; i++) {
        string name, hash;
        float ammount;
        fsu >> name >> hash >> ammount;
        User u{};
        u.setName(name);
        u.setHash(hash);
        u.setBalance(ammount);
        users.push_back(u);
    }

    for (size_t i = 0; i < 10000; i++) {

        string sender, receiver, id;
        time_t date;
        int ammount;
        fst >> sender >> receiver >> ammount >> date >> id;
        string data = sender + receiver + std::to_string(ammount) + std::to_string(date);
        string check = AMHash::getHash(data);
        if (id == check) {
            long su = findUserByHash(users, sender);
            long ru = findUserByHash(users, receiver);
            if (users[su].getBalance() >= ammount) {
                Transaction t{};
                t.setId(id);
                t.setAmmount(ammount);
                t.setDate(date);
                t.setReceiver(receiver);
                t.setSender(sender);
                transactions.push_back(t);
                users[su].setBalance(users[su].getBalance() - ammount);
                users[ru].setBalance(users[ru].getBalance() + ammount);
            }
        }
    }


    std::cout << transactions.size() << std::endl;

    while (!transactions.empty()) {
        vector<Transaction> poolA{};
        vector<Transaction> poolB{};
        vector<Transaction> poolC{};
        vector<Transaction> poolD{};
        vector<Transaction> poolE{};
        if (transactions.size() < 500) {
            auto l = transactions.size() / 5;
            for (size_t j = 0; j < l; j++) {
                poolA.push_back(transactions[j]);
            }
            for (size_t j = 0; j < l; j++) {
                poolB.push_back(transactions[l + j]);
            }
            for (size_t j = 0; j < l; j++) {
                poolC.push_back(transactions[2*l + j]);
            }
            for (size_t j = 0; j < l; j++) {
                poolD.push_back(transactions[3*l + j]);
            }
            transactions.erase(transactions.begin(), transactions.begin() + 4 * l);
            poolE = std::move(transactions);
            transactions.clear();
        } else {
            for (size_t j = 0; j < 100; j++) {
                poolA.push_back(transactions[j]);
            }
            for (size_t j = 0; j < 100; j++) {
                poolB.push_back(transactions[100 + j]);
            }
            for (size_t j = 0; j < 100; j++) {
                poolC.push_back(transactions[200 + j]);
            }
            for (size_t j = 0; j < 100; j++) {
                poolD.push_back(transactions[300 + j]);
            }
            for (size_t j = 0; j < 100; j++) {
                poolE.push_back(transactions[400 + j]);
            }
            transactions.erase(transactions.begin(), transactions.begin() + 500);
            std::cout << transactions.size() << std::endl;
        }
        Block bA(1, poolA);
        Block bB(1, poolB);
        Block bC(1, poolC);
        Block bD(1, poolD);
        Block bE(1, poolE);
        vector<Block> blocksToAdd{bA, bB, bC, bD, bD};
        amc.addBlocks(blocksToAdd, 1000);
    }

    fst.close();
    fsu.close();
    return 0;
}

long findUserByHash(const vector<User> &users, const string& hash) {
    for (int i = 0; i < users.size(); i++){
        if (users[i].getHash() == hash){
            return i;
        }
    }
    return -1;
}
