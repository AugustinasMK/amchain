//
// Created by amake on 19/11/07.
//

#include "../Headers/User.h"

const string &User::getName() const {
    return name;
}

void User::setName(const string &name) {
    User::name = name;
}

const string &User::getHash() const {
    return hash;
}

void User::setHash(const string &hash) {
    User::hash = hash;
}

double User::getBalance() const {
    return balance;
}

void User::setBalance(double balance) {
    User::balance = balance;
}
