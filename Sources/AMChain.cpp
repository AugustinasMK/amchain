//
// Created by amake on 19/10/17.
//

#include "../Headers/AMChain.h"

AMChain::AMChain() {
    Transaction t{};
    t.setSender("Genesis");
    vector<Transaction> genesis{t};
    blocks.emplace_back(Block(0,genesis));
    diff = 3;
}

int AMChain::addBlock(Block newBlock, long maxIterations) {
    newBlock.setPrevHash(getLastBlock().getHash());
    int r = newBlock.MineBlock(diff, maxIterations);
    if (r == -1) {
        return -1;
    } else if (r == 0) {
        blocks.push_back(newBlock);
        return 0;
    }
    return -1;
}

Block AMChain::getLastBlock() const {
    return blocks.back();
}

void AMChain::addBlocks(vector<Block> blocksToAdd, long maxIterations) {
    if (blocksToAdd.empty()){
        return;
    }
    for (size_t i = 0; i < blocksToAdd.size(); i++) {
        auto r = addBlock(blocksToAdd[i], maxIterations);
        if (r == 0){
            blocksToAdd.erase(blocksToAdd.begin() + i);
            addBlocks(blocksToAdd, maxIterations);
            return;
        }
    }
    addBlocks(blocksToAdd, 2 * maxIterations);
}


