# AMChain
Šioje repozitorijoje pateikiamas mano sukurta blockchain implementacija. Parašyta C++ kalba CLion aplinkoje laikantis  C++17 standartų. Šis kodas parašytas VU MIF ISI studijų programos pasirenkamojo dalyko "Blokų grandinių technologijos" [2-ai užduočiai](https://github.com/blockchain-group/Blockchain-technologijos/blob/master/pratybos/2uzduotis-Blockchain.md) atlikti.

## Reikalavimai
### Pagrindiniai
Sukurkite "centralizuotą" blokų grandinę (blockchain'ą) ir susimuliuokite blokų grandinės veikimą kuo natūralesnėmis sąlygomis. Norint tai pasiekti, preliminari veiksmų seka (nebūtinai eilės tvarka, tokia kokia čia nurodyta) galėtų būti tokia:

1. Sugeneruoti ~1000 tinklo vartotojų (aka user'ių), kurie turėtų bent tris atributus: `vardą`, viešąjį _hash_ raktą (`public_key`) ir tam tikros valiutos (galite pasivadinti savo vardu :smile: ) atsitiktinį balansą, pvz. nuo 100 iki 1000000 vienetų.
2. Sugeneruoti tam tikrą skaičių, pvz., transkacijų _pool_'ą sudarytą iš 10000 atsitiktinių transakcijų (jų struktūrą kol kas yra neformalizuota) tarp visų vartotojų, kurių metu jie vienas kitam atlikinėtų tam tikro atsitiktinio dydžio pinigų pervedimus.
3. Iš šių transakcijų atsitiktinai pasirinkti 100-ą (tarsime, kad blokas talpins apie 100 transakcijų) ir jas priskirti naujam sugeneruotam blokui (kurį kitame žingsnyje dar reikės "iškąsti"), kurio struktūra nurodyta paveiksle aukščiau.
4. Realizuoti naujų blokų kasimo (angl. *mining*) Proof-of-Work (PoW) procesą, kurio tikslas yra surasti naujam blokui hash'ą, tenkinantį `Difficulty Targer` reikalavimą, t.y., hash'o pradžioje esančių nulių skaičių.
5. Suradus tokį naujo bloko hash'ą, bloką pridėti prie grandinės. Kartoti 3-5 žingsnius tol, kol yra laisvų transakcijų.

### Reikalavimai versijai (`v0.2`)

- Atlikite transakcijų pool'e esančių transakcijų verifikavimą:
  - **Balanso tikrinimas**: Jeigu Jūsų sukurtoje transakcijoje vartotojas A siunčia tam tikrą pinigų sumą vartotojui B, tačiau jo balansas yra mažesnis negu siunčiama suma, tą transakciją reiktų ištrinti iš transakcijų pool'o.
  - **Transakcijos hash'o tikrinimas**: Jeigu Jūsų transakcijų struktūra neturi transakcijos ID lauko, tuomet papildykite juo! To lauko reikšmė turi būti transakcijos informacijos hash'as. Tuomet poool'o lygmenyje papildomai realizuokite patikrinimą ar tranksakcijos informacijos hash'as sutampa su transakcijos ID. Tokiu būdu įsitikinsite, kad transakcija nebuvo suklastota, kol ji "keliavo" iki transakcijų pool'o.
- Patobulinkite blokų kasimo (_mininimo_) procesą pagal tokią (ar panašią) logiką:
  - Pirmiausiai pagal aukščiau pateiktą aprašą 3-ame žingsnyje iš visų transakcijų _pool_'o, sudarykite ne vieną, o penkis naujus potencialius blokus (kandidatus): 1A, 1B, 1C, 1D, 1E, sudarytus iš 100 atsitiktinai pasirinktų tranksakcijų, kurios blokų kandidatuose gali persidengti.
  - Tuomet atsitiktinai pasirinkite vieną iš tų penkių blokų-kandidatų ir su juo tam tikrą fiksuotą laiką (pvz., iki 1 sek.) arba fiksuojant maksimalų hash'ų bandymų skaičių (pvz. iki 100000) atlikite _mininimo_ procesą kaip aprašyta ankstesnės užduoties 4-ame žingsnyje. Jeigu po to laiko, nebuvo šiam atsitiktinai pasirinktam blokui-kandidatui surastas hash'as, tenkinantis `Difficulty Targer` reikalavimą, tą procesą pakartokite su atsitiktinai pasirinktu vienu iš likusių (keturių, trijų ir t.t.) kandidatų.
  - Jeigu procesą pakartojus su visais penkiais naujais blokais-kandidatais, nebuvo surastas reikiamų savybių hash'as, tuomet pailginkite _mininimo_ procesą (pvz. iki 2 sek.) ar padidinus maksimalų hash'ų bandymų skaičių (pvz. iki 200000) ir pakartokite aukščiau aprašytą procesą.

### Reikalavimai versijai (`v0.3`) (Galutinis terminas: 2019-11-29)
Jeigu dar neturite, realizuokite Merkle medžio hashavimo funkciją.


## Changelog
### v1.0 - 2019-12-12
#### Updated
- Modernized the code, updated ReadMe.md to cover the whole task.
### v0.3 - 2019-11-23
#### Added
- Merkle tree hash
### v0.2
#### Atnaujinta
- Pasiekti v0.2 reikalavimai
### v0.1
#### Pridėta
- Pasiekti pagr. reikalavimai
