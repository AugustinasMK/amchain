//
// Created by amake on 19/10/17.
//

#ifndef AMCHAIN_AMCHAIN_H
#define AMCHAIN_AMCHAIN_H

#include "Block.h"

class AMChain {
public:
    AMChain();
    int addBlock(Block, long);
    void addBlocks(vector<Block>, long);
private:
    unsigned int diff;
    vector<Block> blocks;
    Block getLastBlock() const;
};


#endif //AMCHAIN_AMCHAIN_H
