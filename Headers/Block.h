//
// Created by amake on 19/10/17.
//

#ifndef AMCHAIN_BLOCK_H
#define AMCHAIN_BLOCK_H

#include <string>
#include <vector>
#include "AMHash.h"
#include "Transaction.h"

using std::string;
using std::vector;

class BlockHeader {
public:

    BlockHeader(const unsigned int version);

    const string &getPrevBlockHash() const;

    void setPrevBlockHash(const string &hash);

    time_t getTimestamp() const;

    long getNonce() const;

    unsigned int getVersion() const;

    void setNonce(long n);

    const string &getMerkleRoot() const;

    void setMerkleRoot(const string &merkleRoot);

private:
    string merkle_root;
    string prevBlockHash;
    time_t timestamp;
    long nonce;
    unsigned int version;
};

class BlockBody {
public:
    BlockBody(vector<Transaction> transactions);
    const vector<Transaction> &getTransactions() const;
private:
    vector<Transaction> transactions;
};

class Block {
public:

    Block(const unsigned int version, const vector<Transaction> &transactions);

    int MineBlock(unsigned int diff, long);
    void setPrevHash(const string &hash);

    const string &getHash() const;
    string CalculateHash();

private:
    BlockHeader _header;
    BlockBody _body;
    string hash;

    void createMerkle();

};


#endif //AMCHAIN_BLOCK_H
