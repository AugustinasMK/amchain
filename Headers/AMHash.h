//
// Created by amake on 19/10/17.
//

#ifndef AMCHAIN_AMHASH_H
#define AMCHAIN_AMHASH_H

#include <string>
#include <sstream>

using std::string;

class AMHash{
public:
    static string getHash(const string& input);
private:
    static string make64Bit(unsigned long long charSum, size_t length);
};

#endif //AMCHAIN_AMHASH_H
