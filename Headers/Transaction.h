//
// Created by amake on 19/11/07.
//

#ifndef AMCHAIN_TRANSACTION_H
#define AMCHAIN_TRANSACTION_H


#include "User.h"

class Transaction {
    string id;
    string sender;
    string receiver;
    float ammount;
    time_t date;

public:
    const string &getId() const;

    void setId(const string &id);

    time_t getDate() const;

    void setDate(time_t date);

    const string &getSender() const;

    void setSender(const string &sender);

    const string &getReceiver() const;

    void setReceiver(const string &receiver);

    float getAmmount() const;

    void setAmmount(float ammount);
};


#endif //AMCHAIN_TRANSACTION_H
