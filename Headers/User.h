//
// Created by amake on 19/11/07.
//

#ifndef AMCHAIN_USER_H
#define AMCHAIN_USER_H

#include <string>

using std::string;

class User {
private:
    string name;
    string hash;
    double balance;

public:
    const string &getName() const;

    void setName(const string &name);

    const string &getHash() const;

    void setHash(const string &hash);

    double getBalance() const;

    void setBalance(double balance);
};


#endif //AMCHAIN_USER_H
